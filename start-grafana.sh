#!/usr/bin/sh
# script taken from https://grafana.com/docs/installation/docker/

mkdir -p data/grafana # creates a folder for your data
ID=$(id -u) # saves your user id in the ID variable

# starts grafana with your user id and using the data folder
docker run -d --user $ID --volume "$PWD/data/grafana:/var/lib/grafana" -p 3000:3000 grafana/grafana
