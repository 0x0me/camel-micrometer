package dev.mindcrime.test.camel.camelmicrometer;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.jmx.JmxCollector;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.Route;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.micrometer.messagehistory.MicrometerMessageHistoryFactory;
import org.apache.camel.component.micrometer.messagehistory.MicrometerMessageHistoryService;
import org.apache.camel.component.micrometer.routepolicy.MicrometerRoutePolicyFactory;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.management.MalformedObjectNameException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.camel.builder.ExpressionBuilder.bodyExpression;
import static org.apache.camel.builder.ExpressionBuilder.randomExpression;
import static org.apache.camel.builder.PredicateBuilder.toPredicate;

/**
 * Tiny application to show how to gain metrics from Apache Camel for Prometheus by Micrometer.
 */
@SpringBootApplication
@Slf4j
public class CamelMicrometerApplication {


    public static void main(String[] args) {
        SpringApplication.run(CamelMicrometerApplication.class, args);
    }

    /**
     * Configures camel context to send some route and message history metrics to the micrometer registries.
     */
    @Configuration
    static class CamelContextMetricsConfiguration {
        @Bean
        CamelContextConfiguration contextConfiguration() {
            return new CamelContextConfiguration() {

                @Override
                public void beforeApplicationStart(CamelContext camelContext) {
                    camelContext.addRoutePolicyFactory(new MicrometerRoutePolicyFactory());
                    camelContext.setMessageHistoryFactory(new MicrometerMessageHistoryFactory());
                }

                @Override
                public void afterApplicationStart(CamelContext camelContext) {

                }
            };
        }
    }

    /**
     * Add JMX collector and configure JMX collector to get 'all' camel metrics known by JMX.
     */
    @Configuration
    static class JmxCollectorMetricsConfiguration {

        @Bean
        public CollectorRegistry getCollectorRegistry(ResourceLoader loader) throws MalformedObjectNameException, IOException {
            CollectorRegistry collectorRegistry = CollectorRegistry.defaultRegistry;
            try (InputStream is = loader.getResource("classpath:/collect.yml").getInputStream()) {
                String config = FileCopyUtils.copyToString(new InputStreamReader(is));
                collectorRegistry.register(new JmxCollector(config));
            }
            return collectorRegistry;
        }
    }

    /**
     * Setup some demo routes. One will generate a random number on each tick a forwards it to either the route for
     * processing even numbers or the one to process the odd ones.
     */
    @Component
    static class OddEvenGeneratorDistribtionRoutes extends RouteBuilder {

        @Override
        public void configure() {
            Predicate isEven = toPredicate(bodyExpression(Integer.class, (body) -> body % 2 == 0));

            // generate numbers and distribute based on oddity
            from("timer:generate")
                    .autoStartup(false)
                    .transform(randomExpression(0, 500))
                    .choice()
                    .when(isEven)
                    .to("seda:even")
                    .endChoice()
                    .otherwise()
                    .to("seda:odd")
                    .endChoice()
                    .end()
                    .end()
                    .routeId("GeneratorRoute");


            Processor sleep = exchange -> {
                int value = exchange.getIn().getBody(Integer.class) * 10;
                Thread.sleep(value);
            };

            //yield even numbers
            from("seda:even")
                    .process(sleep).id("delay-even")
                    .to("log:even")
                    .end()
                    .routeId("EvenRoute");

            //yield odd numbers
            from("seda:odd")
                    .process(sleep).id("delay-odd")
                    .to("log:odd")
                    .end()
                    .routeId("OddRoute");
        }
    }

    @RestController
    class RoutesController {
        private final CamelContext context;

        public RoutesController(CamelContext context) {
            this.context = context;
        }

        @GetMapping(path = "/srv/routes", produces = "application/json")
        public Map<String, String> list() {
            return context.getRoutes().stream().collect(Collectors.toMap(Route::getId, r -> context.getRouteStatus(r.getId()).name()));
        }

        @GetMapping(path = "/srv/routes/{routeId}/stop", produces = "application/json")
        public Map<String, String> stop(@PathVariable String routeId) throws Exception {
            context.stopRoute(routeId);
            return Map.of(routeId, context.getRouteStatus(routeId).name());
        }

        @GetMapping(path = "/srv/routes/{routeId}/start", produces = "application/json")
        public Map<String, String> start(@PathVariable String routeId) throws Exception {
            context.startRoute(routeId);
            return Map.of(routeId, context.getRouteStatus(routeId).name());
        }

        @GetMapping(path = "/srv/cm/stats", produces = "application/json")
        public String stats(@RequestParam Map<String,String> allParams) {
            MicrometerMessageHistoryService service = context.hasService(MicrometerMessageHistoryService.class);
            final String statsInJson;

            if (allParams.containsKey("seconds") ) {
                log.info("Dumping stats in seconds");
                statsInJson = service.dumpStatisticsAsJsonTimeUnitSeconds();
            } else {
                log.info("Dumping stats");
                statsInJson = service.dumpStatisticsAsJson();
            }
            return statsInJson;
        }
    }
}
