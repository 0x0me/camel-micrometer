#!/usr/bin/sh
# script taken from https://grafana.com/docs/installation/docker/

mkdir -p data/prometheus # creates a folder for your data
ID=$(id -u) # saves your user id in the ID variable

# starts prometheus with your user id and using the data folder
docker run -d --user $ID  --volume "$PWD/etc/prometheus.yml:/etc/prometheus/prometheus.yml"\
                          --volume "$PWD/data/prometheus:/prometheus" -p 9090:9090 prom/prometheus
